package com.mconnti.library.rest;

import com.mconnti.library.business.UserBO;
import com.mconnti.library.entity.User;
import com.mconnti.library.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/rest/user")
public class UserService {

    @Autowired
    private UserBO userBO;

    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
    public
    @ResponseBody
    MessageReturn save(@RequestBody User user) {
        MessageReturn ret = new MessageReturn();
        try {
            ret = userBO.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	List<User> list() {
//
//		List<User> list = new ArrayList<>();
//		try {
//			list = userBO.list();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return list;
//	}
//
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	List<User> listByUser(@PathVariable Long id) {
//
//		List<User> list = new ArrayList<>();
//		try {
//			list = userBO.listByUser(id);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return list;
//	}
//
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/validatenewuser/{token}", method = RequestMethod.GET, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn validateNewUser(@PathVariable String token) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			ret = userBO.validateNewUser(token);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return ret;
//	}
//
//	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn resetPassword(@FormParam("email") String email, @FormParam("locale") String locale) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			if (locale.equals("pt_BR") || locale.equals("pt")) {
//				locale = "pt_br";
//			} else {
//				locale = "en";
//			}
//			ret = userBO.resetPassword(email, locale);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return ret;
//	}
//
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			for (Long id : ids) {
//				ret = userBO.delete(id);
//			}
//		} catch (Exception e) {
//			ret.setMessage(e.getMessage());
//		}
//		return ret;
//	}
//
//	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn login(@FormParam("username") String username, @FormParam("password") String password) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			ret = userBO.login(username, password);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ret;
//	}
//
//	@Consumes({MediaType.APPLICATION_JSON})
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/emailcheck", method = RequestMethod.PUT, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn emailCheck(@RequestBody User user) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			Map<String, String> queryParams = new LinkedHashMap<String, String>();
//			queryParams.put(" where x.email", "= '" + user.getEmail() + "'");
//
//			ret.setUser(userBO.findByParameter(User.class, queryParams));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ret;
//	}
//
//	@Consumes({MediaType.APPLICATION_JSON})
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/usernamecheck", method = RequestMethod.PUT, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn usernameCheck(@RequestBody User user) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			Map<String, String> queryParams = new LinkedHashMap<String, String>();
//			queryParams.put(" where x.username", "= '" + user.getUsername() + "'");
//
//			ret.setUser(userBO.findByParameter(User.class, queryParams));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ret;
//	}
//
//	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
//	@Produces({MediaType.APPLICATION_JSON})
//	@RequestMapping(value = "/validatetoken", method = RequestMethod.POST, headers = "Accept=application/json")
//	public
//	@ResponseBody
//	MessageReturn getByUsername(@FormParam("token") String token) {
//		MessageReturn ret = new MessageReturn();
//		try {
//			ret = userBO.validateToken(token);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ret;
//	}
}
