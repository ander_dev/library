package com.mconnti.library.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "bookQueue")
public class BookQueue {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_BOOKQUEUE_AUTHOR_ID"))
	private User user;
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity = Book.class)
	@JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_BOOKQUEUE_BOOK_ID"))
	private Book book;
	
	private Date dateIn;
	
	private Boolean renting;
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID: "+getId()).append(" - USER: "+getUser()).append(" - BOOK: "+getBook()).append(" - Date IN: "+getDateIn());
		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Boolean getRenting() {
		return renting;
	}

	public void setRenting(Boolean renting) {
		this.renting = renting;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
}
