package com.mconnti.library.persistence;

import com.mconnti.library.entity.Author;

public interface AuthorDAO extends GenericDAO<Author>{

}
