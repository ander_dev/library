package com.mconnti.library.persistence;

import com.mconnti.library.entity.User;
import com.mconnti.library.entity.xml.MessageReturn;

public interface UserDAO  extends GenericDAO<User>{
	
	public MessageReturn getByEmail(final String email) throws Exception;

}
