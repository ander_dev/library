package com.mconnti.library.persistence;

import com.mconnti.library.entity.Category;

public interface CategoryDAO extends GenericDAO<Category>{

}
