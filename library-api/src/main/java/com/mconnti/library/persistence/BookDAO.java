package com.mconnti.library.persistence;

import com.mconnti.library.entity.Book;

public interface BookDAO extends GenericDAO<Book> {

}
