package com.mconnti.library.persistence.impl;

import com.mconnti.library.persistence.BookQueueDAO;
import org.springframework.stereotype.Repository;

import com.mconnti.library.entity.BookQueue;

@Repository("bookQueueDAO")
public class BookQueueDAOImpl extends GenericDAOImpl<BookQueue> implements BookQueueDAO {

}
