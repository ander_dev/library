package com.mconnti.library.persistence.impl;

import org.springframework.stereotype.Repository;

import com.mconnti.library.entity.Book;
import com.mconnti.library.persistence.BookDAO;

@Repository("bookDAO")
public class BookDAOImpl extends GenericDAOImpl<Book> implements BookDAO{

}
