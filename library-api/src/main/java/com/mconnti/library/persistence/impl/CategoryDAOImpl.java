package com.mconnti.library.persistence.impl;

import com.mconnti.library.persistence.CategoryDAO;
import org.springframework.stereotype.Repository;

import com.mconnti.library.entity.Category;

@Repository("categoryDAO")
public class CategoryDAOImpl extends GenericDAOImpl<Category> implements CategoryDAO {

}
