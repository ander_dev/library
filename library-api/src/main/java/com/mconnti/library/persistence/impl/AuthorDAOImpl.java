package com.mconnti.library.persistence.impl;

import com.mconnti.library.persistence.AuthorDAO;
import org.springframework.stereotype.Repository;

import com.mconnti.library.entity.Author;

@Repository("authorDAO")
public class AuthorDAOImpl extends GenericDAOImpl<Author> implements AuthorDAO {

}
