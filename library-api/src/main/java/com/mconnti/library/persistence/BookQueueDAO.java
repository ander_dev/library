package com.mconnti.library.persistence;

import com.mconnti.library.entity.BookQueue;

public interface BookQueueDAO  extends GenericDAO<BookQueue> {

}
