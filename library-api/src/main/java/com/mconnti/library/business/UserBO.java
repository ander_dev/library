package com.mconnti.library.business;

import com.mconnti.library.entity.User;
import com.mconnti.library.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userBO")
public interface UserBO extends GenericBO<User>{

	List<User> list() throws Exception;

	MessageReturn save(final User user) throws Exception;

	MessageReturn delete(Long id);

	User getById(Long id);

	MessageReturn login(User user);
	
}

