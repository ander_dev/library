package com.mconnti.library.business.impl;

import com.mconnti.library.business.UserBO;
import com.mconnti.library.entity.User;
import com.mconnti.library.entity.xml.MessageReturn;
import com.mconnti.library.persistence.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class UserBOImpl extends GenericBOImpl<User> implements UserBO {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public MessageReturn save(User user) {
		MessageReturn libReturn = new MessageReturn();
		try {
            saveGeneric(user);
        } catch (Exception e) {
			e.printStackTrace();
            libReturn.setUser(user);
            libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && user.getId() == null) {
			libReturn.setMessage(user.getLanguage().equals("pt_BR") ? "Usu�rio Cadastrado com sucesso!" : "User registration successfully!");
            libReturn.setUser(user);
        } else if (libReturn.getMessage() == null && user.getId() != null){
			libReturn.setMessage(user.getLanguage().equals("pt_BR") ? "Usu�rio alterado com sucesso!" : "User updated successfully!");
            libReturn.setUser(user);
        }
		return libReturn;
	}

	@Override
	public List<User> list() throws Exception {
		return list(User.class, null, null);
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		User user = null;
		try {
			user = findById(User.class, id);
			remove(user);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public User getById(Long id) {
		try {
			return findById(User.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public MessageReturn login(User user) {
		MessageReturn messageReturn = null;
		try {
			messageReturn = userDAO.getByEmail(user.getEmail());
			if(messageReturn.getUser() == null){
				messageReturn.setMessage(user.getLanguage().equals("pt_BR") ? "Usu�rio n�o encontrado!" : "User not exists!");
			}else if (!messageReturn.getUser().getPassword().equals(user.getPassword())) {
				messageReturn.setUser(null);
				messageReturn.setMessage(messageReturn.getUser().getLanguage().equals("pt_BR") ? "Senha informada esta incorreta!" : "Informed password is no correct!");
			}else{
				messageReturn.setMessage(messageReturn.getUser().getLanguage().equals("pt_BR") ? "Login realizado com sucesso!" : "Successfully logged!");
			}
		} catch (Exception e) {
			messageReturn = new MessageReturn();
			messageReturn.setMessage(e.getMessage());
		}
		return messageReturn;
	}
}
