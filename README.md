# README #

Library System

System to manage books available for renting from the Library, in case it is not available user is added to a queue that warned the next in line when the book is returned.

For this we need USERS entries, CATEGORIES, AUTHORS, BOOKS AND QUEUE.

How it works:

* User needs to log on the system
* If the login succeed the system will display the list of books, this list will show the status of each book (rented or available)
* User can search books for book name (partial or complete), by author and by category of book
* If the user selects an available book mark this book with rented and forward the user to remove the book on the counter
* If you select a book that is rented
* System will give the option to enter it in a queue, showing the number of users waiting in the queue for get same book
* If selected get in line, you will be notified when the book is returned by the user in the previous row to it.


### How do I get set up? ###

* install MariaDB 5.5 or newer on its default port 3306
    * access mysql using root user and password created
        * mysql - u root -p
        * create user libuser win password libapass
           ** CREATE USER 'libuser'@'localhost' IDENTIFIED BY 'libpass';
        * grant full access to libuser
           ** GRANT ALL PRIVILEGES ON *.* TO 'libuser'@'localhost' WITH GRANT OPTION;
        * create database library
           ** create database library;
           
### Note that if you have changed DB user/pass/url you will need to change jdbc.properties file ###

* jdbc.url=jdbc:mysql://localhost:3306/library
* jdbc.username=libuser
* jdbc.password=libpass