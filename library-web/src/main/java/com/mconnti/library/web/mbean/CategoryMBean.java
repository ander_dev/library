package com.mconnti.library.web.mbean;

import com.mconnti.library.entity.Category;
import com.mconnti.library.entity.xml.MessageReturn;
import com.mconnti.library.util.FacesUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;

@SessionScoped
@ManagedBean(name = "categoryMBean")
public class CategoryMBean implements Serializable {

	private static final long serialVersionUID = 1L;
    Client client;
    String host;
    @ManagedProperty(value = "#{userMBean}")
    private UserMBean userMBean;
	private List<Category> categoryList;

	private Category category;

	private Category[] selectedCategorys;

	public CategoryMBean() {
        client = ClientBuilder.newClient();
        this.category = new Category();
		Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (request instanceof HttpServletRequest) {
			String[] str = ((HttpServletRequest) request).getRequestURL().toString().split("library");
			host = str[0];
		}
	}

    public UserMBean getUserMBean() {
        return userMBean;
    }

    public void setUserMBean(UserMBean userMBean) {
        this.userMBean = userMBean;
    }

	private void loadList(){

        WebTarget webTarget = client.target(host).path("category");

        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

        categoryList = response.readEntity(new GenericType<List<Category>>() {
        });
    }

	public String list() {
		
		loadList();
		
		return "/common/listCategory.xhtml?faces-redirect=true";
	}
	
	public String cancel(){
		return "/common/listCategory.xhtml?faces-redirect=true";
	}

	public void newCategory() {
		this.category = new Category();
	}

	public void edit() {
	}

	public String save() {
		MessageReturn ret = new MessageReturn();
		try {
            WebTarget webTarget = client.target(host).path("category");

			category.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(category, MediaType.APPLICATION_JSON));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);

			FacesUtil.showSuccessMessage(ret.getMessage());
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
		return "/common/listCategory.xhtml?faces-redirect=true";
	}

	public void delete() {
		MessageReturn ret = new MessageReturn();
		try {
			for (Category category : selectedCategorys) {
                WebTarget webTarget = client.target(host).path("category/" + category.getId());

                Response response = webTarget.request().delete();

				if (response.getStatus() != 201 && response.getStatus() != 200) {
					ret.setMessage("Failed : HTTP error code : " + response.getStatus());
					throw new Exception(ret.getMessage());
				}

                ret = response.readEntity(MessageReturn.class);
            }

			if (selectedCategorys.length > 1) {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Categorias excluidos com sucesso!" : "Categories successfully deleted!");
			} else {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Removido com sucesso!" : "Successfully deleted!");
			}
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Category[] getSelectedCategorys() {
		return selectedCategorys;
	}

	public void setSelectedCategorys(Category[] selectedCategorys) {
		this.selectedCategorys = selectedCategorys;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

}
