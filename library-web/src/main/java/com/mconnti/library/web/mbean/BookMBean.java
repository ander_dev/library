package com.mconnti.library.web.mbean;

import com.mconnti.library.entity.Author;
import com.mconnti.library.entity.Book;
import com.mconnti.library.entity.BookQueue;
import com.mconnti.library.entity.Category;
import com.mconnti.library.entity.xml.MessageReturn;
import com.mconnti.library.entity.xml.SearchObject;
import com.mconnti.library.util.FacesUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name = "bookMBean")
public class BookMBean implements Serializable {

	private static final long serialVersionUID = 1L;
    public SelectItem[] authores;
    public SelectItem[] categories;
    @ManagedProperty(value = "#{userMBean}")
    private UserMBean userMBean;
	private List<Book> bookList;
	private List<Author> authorList;
	private List<Category> categoryList;

	private Category category;

	private Author author;

	private Book book;

	private Book[] selectedBooks;

	private String queueTip;

    private Client client;

    private String host;

	public BookMBean() {
        client = ClientBuilder.newClient();
        this.book = new Book();
		author = new Author();
		category = new Category();
		this.book.setAuthor(author);
		this.book.setCategory(category);

		Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (request instanceof HttpServletRequest) {
			String[] str = ((HttpServletRequest) request).getRequestURL().toString().split("library");
            host = str[0] + "libraryAPI";
        }
		loadList();
	}

	private void loadList() {

        WebTarget webTarget = client.target(host).path("book");

        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

        bookList = response.readEntity(new GenericType<List<Book>>() {
        });
	}

	public String list() {

		loadList();

		return "/common/index.xhtml?faces-redirect=true";
	}

	public void getQueueSize() {
		MessageReturn ret = new MessageReturn();
		try {

            WebTarget webTarget = client.target(host).path("bookQueue/listSize");

			SearchObject search = new SearchObject();
			search.getQueryParams().put("book", " = " + book.getId());
			search.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).put(Entity.entity(search, MediaType.APPLICATION_XML));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);
            queueTip = ret.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}
	
	public void insertOnQueue(){
		MessageReturn ret = new MessageReturn();
		try {
            WebTarget webTarget = client.target(host).path("bookQueue");

			BookQueue bookQueue = new BookQueue();
			bookQueue.setBook(book);
            bookQueue.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(bookQueue, MediaType.APPLICATION_JSON));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);

            if (ret.getBookQueue() == null) {
				throw new Exception(ret.getMessage());
			} else {
				FacesUtil.showSuccessMessage(ret.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public void rent(){
		MessageReturn ret = new MessageReturn();
		try {
            WebTarget webTarget = client.target(host).path("bookQueue");

			BookQueue bookQueue = new BookQueue();
			bookQueue.setBook(book);
            bookQueue.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).put(Entity.entity(bookQueue, MediaType.APPLICATION_XML));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);

            if (ret.getBookQueue() == null) {
				throw new Exception(ret.getMessage());
			} else {
				FacesUtil.showSuccessMessage(ret.getMessage());
			}
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}
	
	public void release(){
		MessageReturn ret = new MessageReturn();
		try {
            WebTarget webTarget = client.target(host).path("bookQueue/release");

			BookQueue bookQueue = new BookQueue();
			bookQueue.setBook(book);
            bookQueue.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).put(Entity.entity(bookQueue, MediaType.APPLICATION_XML));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);

            if (ret.getBook() == null) {
				throw new Exception(ret.getMessage());
			} else {
				FacesUtil.showSuccessMessage(ret.getMessage());
			}
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}
	
	public void newBook() {
		this.book = new Book();
		this.author = new Author();
		this.category = new Category();
	}

	public void edit() {
		this.author = book.getAuthor();
		this.category = book.getCategory();
	}
	
	public String cancel(){
		return "/common/index.xhtml?faces-redirect=true";
	}

	public void save() {
		MessageReturn ret = new MessageReturn();
		try {

            WebTarget webTarget = client.target(host).path("book");

            book.setAuthor(author);
            book.setCategory(category);
            book.setAvailable(true);
            book.setUser(userMBean.getLoggedUser());

            Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(book, MediaType.APPLICATION_JSON));


			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

            ret = response.readEntity(MessageReturn.class);

			FacesUtil.showSuccessMessage(ret.getMessage());
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public void delete() {
		MessageReturn ret = new MessageReturn();
		try {
			for (Book book : selectedBooks) {
                WebTarget webTarget = client.target(host).path("book/" + book.getId());

                Response response = webTarget.request().delete();

				if (response.getStatus() != 201 && response.getStatus() != 200) {
					ret.setMessage("Failed : HTTP error code : " + response.getStatus());
					throw new Exception(ret.getMessage());
				}

                ret = response.readEntity(MessageReturn.class);
            }

			if (selectedBooks.length > 1) {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Livros excluidos com sucesso!" : "Books successfully deleted!");
			} else {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Removido com sucesso!" : "Successfully deleted!");
			}
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public SelectItem[] getAuthores() {

        WebTarget webTarget = client.target(host).path("author");

        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

        authorList = response.readEntity(new GenericType<List<Author>>() {
        });

		List<SelectItem> itens = new ArrayList<SelectItem>(authorList.size());

		this.authores = new SelectItem[itens.size()];

		for (Author a : authorList) {
			itens.add(new SelectItem(a.getId(), a.getName()));
		}
		return itens.toArray(new SelectItem[itens.size()]);
	}

    public void setAuthores(SelectItem[] authores) {
        this.authores = authores;
    }

	public SelectItem[] getCategories() {

        WebTarget webTarget = client.target(host).path("category");

        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

        categoryList = response.readEntity(new GenericType<List<Category>>() {
        });

		List<SelectItem> itens = new ArrayList<SelectItem>(categoryList.size());

		this.categories = new SelectItem[itens.size()];

		for (Category c : categoryList) {
			itens.add(new SelectItem(c.getId(), c.getType()));
		}
		return itens.toArray(new SelectItem[itens.size()]);
	}

    public void setCategories(SelectItem[] categories) {
        this.categories = categories;
    }

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Book[] getSelectedBooks() {
		return selectedBooks;
	}

	public void setSelectedBooks(Book[] selectedBooks) {
		this.selectedBooks = selectedBooks;
	}

	public List<Book> getBookList() {
		return bookList;
	}

	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}

	public List<Author> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(List<Author> authorList) {
		this.authorList = authorList;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public String getQueueTip() {
		return queueTip;
	}

	public void setQueueTip(String queueTip) {
		this.queueTip = queueTip;
	}

	public UserMBean getUserMBean() {
		return userMBean;
	}

	public void setUserMBean(UserMBean userMBean) {
		this.userMBean = userMBean;
	}

}
