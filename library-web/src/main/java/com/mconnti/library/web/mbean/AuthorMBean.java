package com.mconnti.library.web.mbean;

import com.mconnti.library.entity.Author;
import com.mconnti.library.entity.xml.MessageReturn;
import com.mconnti.library.util.FacesUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;

@SessionScoped
@ManagedBean(name = "authorMBean")
public class AuthorMBean implements Serializable {

	private static final long serialVersionUID = 1L;
	Client client;
	String host;
	@ManagedProperty(value = "#{userMBean}")
	private UserMBean userMBean;
	private List<Author> authorList;

	private Author author;

	private Author[] selectedAuthors;

	public AuthorMBean() {
		client = ClientBuilder.newClient();
		this.author = new Author();
		Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (request instanceof HttpServletRequest) {
			String[] str = ((HttpServletRequest) request).getRequestURL().toString().split("library");
			host = str[0] + "libraryAPI";
		}
	}

	public UserMBean getUserMBean() {
		return userMBean;
	}

	public void setUserMBean(UserMBean userMBean) {
		this.userMBean = userMBean;
	}

	private void loadList(){
		WebTarget webTarget = client.target(host).path("author");

		Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

		authorList = response.readEntity(new GenericType<List<Author>>() {
		});
	}

	public String list() {
		
		loadList();
		
		return "/common/listAuthor.xhtml?faces-redirect=true";
	}
	
	public String cancel(){
		return "/common/listAuthor.xhtml?faces-redirect=true";
	}

	public void newAuthor() {
		this.author = new Author();
	}

	public void edit() {
	}

	public void save() {
		MessageReturn ret = new MessageReturn();
		try {

			WebTarget webTarget = client.target(host).path("author");

			author.setUser(userMBean.getLoggedUser());

			Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(author, MediaType.APPLICATION_JSON));

			if (response.getStatus() != 201 && response.getStatus() != 200) {
				ret.setMessage("Failed : HTTP error code : " + response.getStatus());
				throw new Exception(ret.getMessage());
			}

			ret = response.readEntity(MessageReturn.class);

			FacesUtil.showSuccessMessage(ret.getMessage());
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public void delete() {
		MessageReturn ret = new MessageReturn();
		try {
			for (Author author : selectedAuthors) {

				WebTarget webTarget = client.target(host).path("author/" + author.getId());

				Response response = webTarget.request().delete();

				if (response.getStatus() != 201 && response.getStatus() != 200) {
					ret.setMessage("Failed : HTTP error code : " + response.getStatus());
					throw new Exception(ret.getMessage());
				}

				ret = response.readEntity(MessageReturn.class);
			}

			if (selectedAuthors.length > 1) {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Autores excluidos com sucesso!" : "Authores successfully deleted!");
			} else {
				FacesUtil.showSuccessMessage(userMBean.getLoggedUser().getLanguage().equals("pt_BR") ? "Removido com sucesso!" : "Successfully deleted!");
			}
			loadList();
		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.showAErrorMessage(ret.getMessage());
		}
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Author[] getSelectedAuthors() {
		return selectedAuthors;
	}

	public void setSelectedAuthors(Author[] selectedAuthors) {
		this.selectedAuthors = selectedAuthors;
	}

	public List<Author> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(List<Author> authorList) {
		this.authorList = authorList;
	}

}
